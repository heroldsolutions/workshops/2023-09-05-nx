import { Component, Input } from "@angular/core";


@Component({
  selector: 'input-text',
  standalone: true,
  imports: [],
  template: `<input [placeholder]="placeholder"/>`
})
export class InputTextComponent {
  @Input() public placeholder = "";
}
