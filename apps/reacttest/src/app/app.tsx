// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { helloWorld } from "@myLibs/util";
import styles from './app.module.css';

import NxWelcome from './nx-welcome';

export function App() {

  helloWorld('myname');
  return (
    <div>
      <NxWelcome title="reacttest" />
    </div>
  );
}

export default App;
