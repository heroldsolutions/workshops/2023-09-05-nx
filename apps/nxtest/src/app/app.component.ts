import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { InputTextComponent } from '@myLibs/inputs';
import { helloWorld } from '@myLibs/util';

@Component({
  standalone: true,
  imports: [RouterModule, InputTextComponent],
  selector: 'nxtest-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'nxtest';

  constructor() {
    helloWorld('myname');
  }
}
